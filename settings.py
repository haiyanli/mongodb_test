# -*- coding: utf-8 -*- 
import datetime

#filesize  unit K,you can update this arg only 1,5,10
FILESIZE = 1
#batch number: this arg means you can batch input 1000 per insert option
BATCHNUM = 100
#sinsernum: this arg means when you do single insert,you will insert SINERTNUM datas totle
SINSERTNUM = 5000
#minsertnum: this arg means when you do mulity process insert,you will insert MINSERTNUM datas per process
MINSERTNUM = 500
#processnum: this arg means when you do any mulity process option,server will create PROCESSNUM process for your script
PROCESSNUM = 10
#insertop: these args means this script will do which option such as insert,update,delete,search
INSERTOP = False
UPDATEOP = False
DELETEOP = True
SEARCHOP = False
#mulityprocess: this arg means when you run this script ifwether use mulity process
MULITYPROCESS = True
#batchoption: this arg means when you run this script ifwether use batch
BATCHOPTION = False
#condition: this arg is a datetime for 'insertime' and means when you do option for update,search,delete ,you will give me a key value for the search condition
CONDITION = datetime.date.today().strftime('%Y-%m-%d')