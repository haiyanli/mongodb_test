# -*- coding: utf-8 -*- 

import pymongo
import datetime
import threading
import multiprocessing
import time
import random

import contents
import settings

con = pymongo.Connection()
db = con.singledata
posts = db.posts

sinsert_num = settings.SINSERTNUM
minsert_num = settings.MINSERTNUM
processes_num = settings.PROCESSNUM
filesize = settings.FILESIZE

name = contents.NAME1
title = contents.TITLE1
content = contents.CONTENT1
comments = contents.COMMENTS1

def getStaticContene():
    if filesize == 1:
        name = contents.NAME1
        title = contents.TITLE1
        content = contents.CONTENT1
        comments = contents.COMMENTS1
    elif filesize == 5:
        name = contents.NAME5
        title = contents.TITLE5
        content = contents.CONTENT5
        comments = contents.COMMENTS5
    elif filesize == 10:
        name = contents.NAME10
        title = contents.TITLE10
        content = contents.CONTENT10
        comments = contents.COMMENTS10

def singleDataInsert():
    getStaticContene()
    for d in xrange(minsert_num):
        posts.save(
            {
                'name':name,
                'title':title,
                'content':content,
                'inserttime':(datetime.datetime.now() - datetime.timedelta(days = random.randrange(1825))).strftime('%Y-%m-%d %H:%M:%S'),
                'comments':comments
            })

def batchDataInsert(batch_num):
    batch_list = []
    getStaticContene()
    for d in xrange(minsert_num):
        batch_list.append({
                'name':name,
                'title':title,
                'content':content,
                'inserttime':(datetime.datetime.now() - datetime.timedelta(days = random.randrange(1825))).strftime('%Y-%m-%d %H:%M:%S'),
                'comments':comments
            })
        if d != 0 and d % batch_num == batch_num - 1:
            posts.insert(batch_list)
            batch_list = []
            
    if batch_list:
        posts.insert(batch_list)    

def singleDataUpdate():
    posts.update({'inserttime':settings.CONDITION},{'$set':{'content':contents.UPDATEDATA}},False,True)

def singleDataDelete():
    posts.remove({'$lt':{'inserttime':settings.CONDITION}})

def singleDataSearch():
    posts.find({'inserttime':settings.CONDITION})

def dataopMultiProcess(func,args):
    pool = multiprocessing.Pool(processes=processes_num)
    for i in xrange(10):
        pool.apply_async(func, args)
        time.sleep(1)
    pool.close()
    pool.join()

if __name__ == '__main__':
    batch_num = settings.BATCHNUM
    if settings.INSERTOP:
        if settings.MULITYPROCESS and not settings.BATCHOPTION:
            print 'singleMultiThread start at:',datetime.datetime.now()
            dataopMultiProcess(singleDataInsert,())
            print 'singleMultiThread end at:',datetime.datetime.now()
            print '================================='
        elif not settings.MULITYPROCESS and not settings.BATCHOPTION:
            print 'singleDataInsert start insert time:%s'%datetime.datetime.now()
            singleDataInsert()
            print 'singleDataInsert end insert time:%s'%datetime.datetime.now()
            print '================================='
        elif not settings.MULITYPROCESS and settings.BATCHOPTION:
            print 'batchDataInsert start insert time:%s'%datetime.datetime.now()
            batchDataInsert(batch_num)
            print 'batchDataInsert end insert time:%s'%datetime.datetime.now()
            print '================================='
        elif settings.MULITYPROCESS and settings.BATCHOPTION:
            print 'batchMultiThread start at:',datetime.datetime.now()
            dataopMultiProcess(batchDataInsert,(batch_num,))
            print 'batchMultiThread end at:',datetime.datetime.now()
            print '================================='
    elif settings.UPDATEOP:
        if settings.MULITYPROCESS:
            print 'update MultiProcess start at:',datetime.datetime.now()
            dataopMultiProcess(singleDataUpdate,())
            print 'update MultiProcess end at:%s'%datetime.datetime.now()
            print '================================='
        else:
            print 'singleDataUpdate start at:',datetime.datetime.now()
            singleDataUpdate()
            print 'singleDataUpdate end at:',datetime.datetime.now()
            print '================================='
    elif settings.DELETEOP:
        if settings.MULITYPROCESS:
            print 'delete MultiProcess start at:',datetime.datetime.now()
            dataopMultiProcess(singleDataDelete,())
            print 'delete MultiProcess end at:%s'%datetime.datetime.now()
            print '================================='
        else:
            print 'singleDataDelete start at:',datetime.datetime.now()
            singleDataDelete()
            print 'singleDataDelete end at:',datetime.datetime.now()
            print '================================='
    elif settings.SEARCHOP:
        if settings.MULITYPROCESS:
            print 'search MultiProcess start at:',datetime.datetime.now()
            dataopMultiProcess(singleDataSearch,())
            print 'search MultiProcess end at:%s'%datetime.datetime.now()
            print '================================='
        else:
            print 'singleDataSearch start at:',datetime.datetime.now()
            singleDataSearch()
            print 'singleDataSearch end at:',datetime.datetime.now()
            print '================================='
